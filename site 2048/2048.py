#Bibliothèques graphique (natives avec python 3)
from tkinter import *
from tkinter.messagebox import *
import random
#Bibliothèque de l'aléa (native avec python 3)
#Pour appeler un nombre entier au hasard entre a et b 
#on fait poetiquement : randint(a, b)
from random import randint
from winsound import * 

auteur="Hajar Oumerri"

"""
*********************************************************************************
*																				*
*							PROGRAMMATION DU JEU 2048							*
*																				*
*********************************************************************************
	Mais bien sur que vous connaissez ce jeu dont l'objectif est d'arriver 
	à la puissance 11 du nombre préféré d'un informaticien : 2048 (2^11).
	
	Lorsque la flèche gauche est utilisée, tous les nombres se retrouvent projeter
	sur la gauche et une règle s'applique : après ce mouvement tous les nombres identiques 
	sur la gauche fusionnent et passent à la puissance supérieur. Voici des exemples : 
		
		2 4 X 4 8 X <-
			deviens : 2 8 8 X X X
			(hé oui, le 8 est crée mais il n'a pas été encore poussé pour fusionner avec l'autre)
			
		2 2 X 2 X 2 <-
			deviens : 4 4 X X X X
			(même principe, les 4 ne peuvent pas fusionner parce qu'ils viennent d'apparaitre)
			
		16 16 16 X X X <-
			deviens : 32 16 X X X X
			(quand il y a "égalité" on va dire que c'est contre le rebord que ça fusione)
			(cela ne fait donc pas 16 32 X X X X)
			
	Bien sur c'est le même principe par un mouvement sur la droite, en haut et en bas.
	
	La partie se termine dès qu'on atteint le nombre 2048 (et on gagne) 
	ou si plus aucun mouvement n'est possible (et on perd) et que la grille est complète !
		
	Pour que ça soit sympa, l'interface graphique est donné (gratuit, c'est bibi qui offre)
	Ceci implique QUE VOUS NE POUVEZ PAS SUPPRIMER LES FONCTIONS CI-DESSOUS.
	Mais rien ne vous interdit de les modifier (d'ailleurs il FAUT les modifier)
	Une seule exception : PAS TOUCHE A LA FONCTION 'FENETRE' qui gère la fenètre graphique !
	Pour le reste : 
		- a vous de modifier les autres fonctions existantes (sans modifier le type d'entrée et de sortie)
		- d'introduire d'autre fonctions ou procédures (autant que vous voulez, aucune restriction)
		- de divisier ce projet en plein de sous tache peut-etre plus facile
		- de commenter votre code (même pour vous ça sera plus confortable)
		
	Allez ! Une petite pièce SOS pour vous guider : 
		la grille de jeu sera assimillé à un tableau à deux entrées 
							grille[i][j]
		le premier indice (i) correspondant à la ligne (de haut en bas)
		le second indice (j) correspondant à la colonne (de gauche à droite)
		Une case vide sera représenté par le nombre 0 (l'interface ne l'affiche pas)
		pour faire simple tous les tableaux seront des dictionnaires 
		(comme ça on se prend pas la tête à jouer avec la mémoire)

"""""
#Initialisation : début de partie
#Il y a trois ou quatres valeurs (en général 2 mais parfois, rarement, un 4)

def Ajout(X):  #cette fonction prend le rôle du joueur 'ordinateur' qui va ajouter une tuile après chaque action du joueur ayant engendré un mouvement sur la grille.
	dim=len(X)  #variable qui renvoie la taille de la grille	
	liste_choix_possible=[] #on crée une liste vide
	
	#on parcours la grille, ensuite si il y'a une case vide alors les coordonnées de cette dernière seront copiées dans la liste 
	for i in range(dim):
		for j in range(dim):
			if X[i][j]==0:
				liste_choix_possible.append([i,j])

		#on crée une variable qui renvoie la taille de la liste crée
		#si la liste ne contient aucune case alors retourner la grille sinon on crée une variable qui renvoie un nombre au hasard  de la taille de la liste
		#ensuite on crée une autre variable qui renvoie la tuile  qui a été stockée auparavant
		#puis enfin on crée 2 variables qui renvoies la ligne et colonne de la variable prècedente pour pouvoir y ajouter soit des 2 soit des 4
				
		taille_liste=len(liste_choix_possible)
		if taille_liste==0:
			return X
		else:
			hasard=randint(0,taille_liste-1)
			cellule=liste_choix_possible[hasard]
			i=cellule[0]
			j=cellule[1]
			
	X[i][j]=random.choice([2,2,2,2,2,2,2,2,2,2,2,4])

	return X


def Init(dim) :   #permet de commencer le jeu soit deux ou trois tuiles qui portent soit les valeurs 2 ou 4 placées aleatoirement

        #copie la grille de jeu
	X=dict()
	for i in range(dim) :
		X[i]=dict()
		for j in range(dim):
			X[i][j]=0

	#la variable permet de parcourir chaque tuile de la grille et si elle est diffèrente de 0 alors il faut placer une valeur aléatoire (2 ou 4)
	#la probabilité que la valeur ajouté soit un 2 est superieur à celle du 4
	var=0
	tuile= random.choice([3,4])
	for var in range(tuile):
		i = randint(0,dim-1)
		j = randint(0,dim-1)
		while X[i][j] != 0:
			i = randint(0,dim-1)
			j = randint(0,dim-1)
			
			X[i][j]=random.choice([2,4])
			break
		X[i][j]=random.choice([2,2,2,2,2,2,2,2,2,2,2,4])
		
	

	return X


#Renvoie 1 si c'est gagné
#Renvoie -1 si c'est perdu
#Renvoie 0 sinon

def TestFin(grille) : #permet de tester si c'est la fin du jeu

	#ceci permet de copier la grille
	dim = len(grille)   
	X = dict()
	for i in range(dim) :
		X[i]=dict()
		for j in range(dim) :
			X[i][j]=grille[i][j]
			
	#il faut parcourir la grille à chaque fois
	#si une case est égale à 2048 la partie est gagnée
	for i in range(dim):
		for j in range(dim):       
			if X[i][j] == 2048 :       
				return 1

	# on sort de la boucle puisqu'on a pas rencontré de 2048
	# on crée un compteur qui va compter le nombre de cases remplies
	# si il ya une case qui est vide continuer à jouer sinon +1 pour le compteur
	
	cases_remplies=0
	for i in range(dim):         
		for j in range(dim):
			if X[i][j] ==0:        
				return 0
			else:
				cases_remplies+=1

	
	# si le compteur est égale au nombre de cases totales et que les mouvements
	# du debut et de la fin ne sont pas égaux alors c'est perdu, sinon continuer à jouer
	
	for i in range(dim):
		for j in range(dim):

			if cases_remplies == dim*dim and ActionHaut(grille)==X and ActionGauche(grille)==X and ActionDroite(grille)==X and ActionBas(grille)==X:
				return -1
			else:
				return 0
					

#------------------------------------------------------------------------------FONCTIONS DE FUSION------------------------------------------------------------------------------------------------------#
#pour toutes les fonctions fusions 
#En entrée : la grille avant le mouvement
#En sortie : la grille après le mouvement
			
def FusionDroite(grille): #fonction pour additionner 2 tuiles lors d'un déplacement vers la droite

        #ceci permet de copier la grille
	dim=len(grille) 
	X = dict()
	for i in range(dim) :
		X[i]=dict()
		for j in range(dim) :
			X[i][j]=grille[i][j]


	#pour la fusion droite on commence à parcourir la grille par la fin en partant de la droite
	#si les deux valeurs successives sont égales alors la dernière case sera égale à elle meme *2, et l'avant dernière case sera égale à 0
	#et on continue ainsi pour toute la grille, et à la fin on appelle la fonction ajout
	
	i=0
	
	while i<dim:
		j=dim-1
		while j !=0:
			if X[i][j] == X[i][j-1]:
				X[i][j]= X[i][j] *2
				X[i][j-1]=0

			#Lors des calcul si une valeurs est mise à zero on effectue le décalage de la tuile d'avant
			if j < dim - 1:
				while X[i][j] != 0 and X[i][j + 1] == 0:
					X[i][j + 1] = X[i][j]
					X[i][j] = 0
					j +=1
			j-=1
			
		i+=1

	Ajout(X)
	return X


def FusionGauche(grille): #fonction pour additionner 2 tuiles lors d'un déplacement vers la gauche

        #ceci permet de copier la grille
	dim=len(grille)  
	X = dict()
	for i in range(dim) :
		X[i]=dict()
		for j in range(dim) :
			X[i][j]=grille[i][j]
			
        #la fusion gauche se base sur le même principe que la fusion droite, sauf que cette fois-ci on commence à parcourir la grille par la gauche 
        
	for execute in range(dim):
		i=0
		while i<dim:
			j=1
			while j <dim:
				if X[i][j-1] == X[i][j]:
					X[i][j-1] = X[i][j-1]*2
					X[i][j] = 0
				if X[i][j-1] == 0 and X[i][j] != 0:
					X[i][j - 1] = X[i][j]
					X[i][j] = 0

				j+=1
			i+=1
	
	Ajout(X)
	return X

	
		

def FusionHaut(grille):  #fonction pour additionner 2 tuiles lors d'un déplacement vers le haut

        #ceci permet de copier la grille
	dim=len(grille) 
	X = dict()
	for i in range(dim) :
		X[i]=dict()
		for j in range(dim) :
			X[i][j]=grille[i][j]

	#pour la fusion haut on commence à parcourir la grille par le haut de la grille
	#en partant du haut, si les deux valeurs successives sont égales alors la case tout en haut sera égale à elle meme *2, et l'avant dernière case sera égale à 0
	#et on continue ainsi pour toute la grille, et à la fin on appelle la fonction ajout
	i= 1
	j= 0
	while i < dim:
		for j in range(dim) :
			if X[i-1][j] == X[i][j]:
				X[i-1][j] = X[i-1][j]*2
				X[i][j] = 0

			#Lors des calcul si une valeurs est mise à zero on effectue le décalage de la tuile
			if X[i][j] != 0 and X[i -1][j] == 0:
				X[i-1][j], X[i][j] = X[i][j], X[i-1][j]
		i+=1
		
	Ajout(X)
	return X


def FusionBas(grille): #fonction pour additionner 2 tuiles lors d'un déplacement vers le bas

        #ceci permet de copier la grille
	dim=len(grille)  
	X = dict()
	for i in range(dim) :
		X[i]=dict()
		for j in range(dim) :
			X[i][j]=grille[i][j]

        #la fusion bas se base sur le même principe que la fusion haut, sauf que cette fois-ci on commence à parcourir la grille par le bas de la grille

	for a in range(dim):
		i = dim-2
		j=0
		while i >= 0 :
			for j in range(dim):
				if X[i+1][j] == X[i][j]:
					X[i+1][j] =X[i+1][j] *2
					X[i][j]=0
				if X[i][j] != 0 and X[i+1][j] == 0:
					X[i+1][j], X[i][j] = X[i][j], X[i + 1][j]

			i-=1
		
	
	Ajout(X)
	return X

	


#------------------------------------------------------------------FONCTIONS DE DEPLACEMENT--------------------------------------------------------------------------------------------------------------#
                
#Renvoie la grille mise à jour après un mouvement vers le haut
#En entrée : la grille avant le mouvement
#En sortie : la grille après le mouvement
def ActionHaut(grille) :
        
        #ceci permet de copier la grille
	dim=len(grille)   
	X=dict()
	for i in range(dim) :
		X[i]=dict()
		for j in range(dim) : 
			X[i][j]=grille[i][j]
	
        #Ici je parcours le tableau en commençant par l'avant dernière ligne en partant par le haut, donc si la la premiere case de la dernière ligne est égale à 0
        #et que la premiere case de l'avant dernière ligne est differente de 0 alors je switch les valeurs des deux cases
		#j'appelle à la fin la fonction fusion haut
			
	for execute in range(dim):
		i = 1
		j = 0   
		while i < dim :   #Pour parcourir chaque ligne
			for j in range(dim) :    #Pour parcourir chaque colonne
				if X[i-1][j] == 0 and X[i][j] != 0 :
					X[i-1][j], X[i][j] = X[i][j], X[i-1][j]
			i+=1
			
	
	
	
	
	return(FusionHaut(X))
	
	


#Renvoie la grille mise à jour après un mouvement vers le bas
#En entrée : la grille avant le mouvement
#En sortie : la grille après le mouvement	
def ActionBas(grille) :

        #ceci permet de copier la grille
	dim=len(grille)    
	X=dict()
	for i in range(dim) :
		X[i]=dict()
		for j in range(dim) : 
			X[i][j]=grille[i][j]
			
         #le deplacement bas se base sur le même principe que le déplacement haut, sauf que cette fois-ci on commence à parcourir la grille par l'avant dernière ligne en partant par le bas de la grille
	
	for execute in range(dim):
		i = dim-2  #j'initialise le i à partir de l'avant denière ligne de la grille
		j=0    #j'initialise le j à partir de la première case à chaque ligne qu'il va explorer
		while i >= 0 :    #Pour parcourir chaque colonne
			for j in range(dim):   #Pour parcourir chaque ligne
				if X[i+1][j] == 0 and X[i][j] != 0 :
					X[i+1][j], X[i][j] = X[i][j], X[i+1][j]
			i-=1

	
	
	return(FusionBas(X))
	
	
        
		
#Renvoie la grille mise à jour après un mouvement vers la gauche
#En entrée : la grille avant le mouvement
#En sortie : la grille après le mouvement
def ActionGauche(grille) :
        #ceci permet de copier la grille
	dim=len(grille)
	X=dict()
	for i in range(dim) :
		X[i]=dict()
		for j in range(dim) : 
			X[i][j]=grille[i][j]
				
	#Ici je parcours le tableau en commençant par la premiere case de la deuxième ligne en partant par la gauche, donc si cette dernière est égale à 0
        #et que la case suivante est differente de 0 alors je switch les valeurs des deux cases
	#j'appelle à la fin la fonction fusion gauche

	for execute in range(dim):
		i = 0
		while i < dim:  #Pour parcourir chaque colonne
			j = 1  #j'initialise le j à partir de la deuxième case à chaque ligne qu'il va explorer
			while j < dim:   #Pour parcourir chaque ligne
				while X[i][j] != 0 and X[i][j-1] == 0:
					X[i][j-1] = X[i][j]
					X[i][j] = 0
					j+= 1
					if j == dim:
						break
				j += 1
			i+=1
			

	return(FusionGauche(X))
	


	
#Renvoie la grille mise à jour après un mouvement vers la droite
#En entrée : la grille avant le mouvement
#En sortie : la grille après le mouvement
def ActionDroite(grille) :
       #ceci permet de copier la grille
        dim=len(grille)
        X=dict()
        for i in range(dim) :
                X[i]=dict()
                for j in range(dim) :
                        X[i][j]=grille[i][j]
        
	#le deplacement droite se base sur le même principe que le déplacement gauche
        #sauf que cette fois-ci on commence à parcourir la grille par l'avant dernière case en partant par la droite de la grille		
	
        for execute in range(dim):
                i = 0
                while i != dim: #Pour parcourir chaque colonne
                        j = dim - 2 #j'initialise le j à l'avant dernière case à chaque ligne qu'il va explorer
                        while j >= 0:   #Pour parcourir chaque ligne
                                while X[i][j] != 0 and X[i][j + 1] == 0:
                                        X[i][j + 1] = X[i][j]
                                        X[i][j] = 0
                                        j += 1
                                        if j == dim - 1:
                                                break
                                j -= 1
                        i+=1

        
       
        return(FusionDroite(X))
        

        

        
	
#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#musique_menu = PlaySound("divers/Light_Of_The_Seven.wav", SND_FILENAME | SND_ASYNC)
def FENETRE(dim) :

	def recupPartie() :
		dim=len(JEU)
		X=dict() 
		for i in range(dim) :
			X[i]=dict()
			for j in range(dim) :
				try : X[i][j]=int(JEU[i][j].get())
				except : X[i][j]=0
		return X
		
	def InjectionPartie(X) : 
		for i in range(dim) :
			for j in range(dim) :
				try : (JEU[i][j]).set(X[i][j])
				except : (JEU[i][j]).set(0)
				
				if(int(JEU[i][j].get())==0) : (JEU[i][j]).set("")

	def Clavier(mouvement):
		CMPT.set(CMPT.get()+1)
		
		touche = mouvement.keysym
		# déplacement vers le haut
		if touche == 'Up': ActionHaut0()
		# déplacement vers le bas
		if touche == 'Down':ActionBas0()
		
		# déplacement vers la droite
		if touche == 'Right':ActionDroite0()

		# déplacement vers la gauche
		if touche == 'Left':ActionGauche0()
			

		
	def ActionRecommencer() :
		X=Init(dim)
		for i in range(dim) :
			for j in range(dim) : 
				(JEU[i][j]).set(X[i][j])
				if(X[i][j]==0) : (JEU[i][j]).set("")
		CMPT.set(0)
		
	def ActionHaut0() :
		InjectionPartie(ActionHaut(recupPartie()))
		TestFin0(recupPartie())
			
	def ActionBas0() :
		InjectionPartie(ActionBas(recupPartie()))
		TestFin0(recupPartie())
		
	def ActionGauche0() :
		InjectionPartie(ActionGauche(recupPartie()))
		TestFin0(recupPartie())
			
	def ActionDroite0() :
		InjectionPartie(ActionDroite(recupPartie()))
		TestFin0(recupPartie())
	
	def TestFin0(X) :			
		test=TestFin(X)
		if(test==1) : 
			musique_gagner = PlaySound("Aya Nakamura - Pookie.wav", SND_FILENAME | SND_ASYNC)
			showinfo("FIN DE PARTIE", "Bravo ! Vous avez gagné en "+str(CMPT.get())+" coups. Recommencer ?")
		if(test==-1) :
			musique_perdu = PlaySound("8057.wav", SND_FILENAME | SND_ASYNC)
			showinfo("FIN DE PARTIE", "Perdu ! Il vous a fallu "+str(CMPT.get())+" coups pour perdre... comment dire. On s'arrête là ou on recommence ?")
		if(test!=0) : ActionRecommencer()


	musique_debut = PlaySound("djadja.wav", SND_FILENAME | SND_ASYNC)
	fenetre = Tk()
	fenetre.title('2048 par '+auteur)
	
	
	CMPT=IntVar()
	CMPT.set(0)
	
	X=Init(dim)
	JEU=dict()
	for i in range(dim) :
		JEU[i]=dict()
		for j in range(dim) : 
			JEU[i][j]=StringVar()
			(JEU[i][j]).set(X[i][j])
			if(X[i][j]==0) : (JEU[i][j]).set("")
			
	base=70 #Taille en px d'un carré
	if(dim>7) : base=50
	marge=10 #Marge de beauté

	hauteur = dim*base+2*marge+100
	largeur = dim*base+2*marge
	

	canvas = Canvas(fenetre, background="#ccc0d3", width=largeur, height=hauteur )

	case=dict()
	for i in range(dim) :
		case[i]=dict()
		for j in range(dim) : 
			canvas.create_rectangle((base*i+marge,base*j+marge), (base*(i+1)+marge,base*(j+1)+marge), width=6,outline='#cbb4d4')
                                        

			c_fg='#000428'
			c_bg ='#ccc0d3' 
			
			if((JEU[i][j]).get()==0) : c_fg='#ccc0d3'
			
			L=Label(fenetre, textvariable=JEU[i][j], fg=c_fg, bg=c_bg)
			L.place(x=base*j+base//2, y=base*i+base//2, anchor="nw")
			

	txt="Score : "
	L=Label(fenetre, text=txt, fg='black', bg='white', relief=RAISED)
	L.place(x=marge, y=(hauteur-base), anchor="sw")
	L=Label(fenetre, textvariable=CMPT, fg='black', bg='white', relief=RAISED)
	L.place(x=marge+len(txt)*7, y=(hauteur-base), anchor="sw")

	fenetre.geometry(str(largeur)+"x"+str(hauteur))
	BoutonQuitter = Button(fenetre, text ='Quitter',command = fenetre.destroy ,relief=RAISED)
	BoutonQuitter.place(x=marge, y=hauteur-marge, anchor="sw")
	BoutonRecommencer = Button(fenetre, text ='Recommencer', command = ActionRecommencer, relief=RAISED)
	BoutonRecommencer.place(x=largeur-marge, y=hauteur-marge, anchor="se")
	
	BoutonBas= Button(fenetre, text = 'Bas', relief=GROOVE, command= ActionBas0)
	BoutonBas.place(x= largeur-marge-70, y= hauteur-marge-40, anchor="se")
	BoutonHaut= Button(fenetre, text = 'Haut', relief=GROOVE, command= ActionHaut0)
	BoutonHaut.place(x= largeur - marge-70, y= hauteur-marge-80, anchor="se")
	BoutonGauche= Button(fenetre, text = 'Gauche', relief=GROOVE, command= ActionGauche0)
	BoutonGauche.place(x= largeur - marge-105, y= hauteur-marge-60, anchor="se")
	BoutonDroite= Button(fenetre, text = 'Droite', relief=GROOVE, command= ActionDroite0)
	BoutonDroite.place(x= largeur - marge-20, y= hauteur-marge-60, anchor="se")
	

	canvas.focus_set()
	canvas.bind('<Key>',Clavier)
	
	canvas.grid()
	fenetre.mainloop()
	
#Dimension du 2048 - usuellement 4
dim=0
while(dim<3 or dim>10) :
	try : dim=int(input("Quelle taille votre 2048 (entre 3 et 10): "))
	except : dim=0
	
#Fonction principale	
FENETRE(dim)


"""
*********************************************************************************
*																				*
*					PROGRAMMATION DU JEU 2048 - EVOLUTION						*
*																				*
*********************************************************************************
	Hein ! Quoi ! Vous avez fini en avance et vous voulez avancer un petit
	peu plus ! Génial. Si vous alliez explorer les tutoriels d'explications 
	de cette étrange bibliothèque tkinter... peut-être que vous trouverez
	un truc pour rajouter un peu de couleur ou une petite musique de fond ^_^
	A vous de voir !
	
	Hé sinon, si au lieu de faire apparaitre des puissances de 2 succéssives
	on mettait les termes de la suite de Fibonacci (1 2 3 5 8 13 etc)
	Et si on laissait l'utilisateur choisir son mode de jeu : 
	puissance de 2 ou suite de Fibonacci ;-)
	
	Qu'est-ce qu'on pourrait encore améliorer !?
	A vous de me faire une surpise, mais n'oubliez pas de préparer la
	page web pour la soutenance !

"""
